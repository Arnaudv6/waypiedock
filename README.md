# waypiedock
<img src="screenshot.png">

A launcher dock, in the shape of a pie, under your mouse. i.e. a wayland piedock.

## Why?

I love [piedock](https://github.com/markusfisch/PieDock/), by Markus. And as much as I am excited about wayland...  
I needed a replacement for piedock before making this transition.  

Piedocks are sexy, and allow for a perfect, balanced Workflow to me. Here is my rationale:  
 * `krunner` sibblings: I've tried them all. They're great, but one quickly depends so much on them he'd replace even the file-manager with it.
 * And as with the `dmenu`-likes, you got to have two hands on the keyboard. (1)
 * `Latte Dock` and the likes: I've tried much. The grand mouse displacement required to reach for wanted item is a pain on the long run.  
    I did not see this at first, I see this now that I use Piedock.
 * Piedocks are great: with my left hand on keyboard I press a keybinding, and it pops under the mouse, with a sexy UI that requires the smallest movement possible to reach your aim. I am in love.

(1) Sorry with hardcode coders here, but I don't join the cause: one does not spend the day with both hands on keyboard. One spends half the day browsing the web. And as much as I would like a web with no javascript menus, no media tags and all, given what the web is, I don't fall for keyboard driven browsers. I also use the GIMP, and many software with one hand on the mouse.

## Howto?

### Foreword
waypiedock (sort of) works with wlroots based wayland compositors.  
Dependencies: python, pygobject, and foreign-toplevel:  
https://gitlab.freedesktop.org/wlroots/wlroots/-/blob/master/examples/foreign-toplevel.c

### Manual installation
Place the script along your $PATH (i.e. in `~/.local/bin/`) and make it executable.

### Installation with pip
`pip install --user --upgrade https://gitlab.com/Arnaudv6/waypiedock/-/archive/master/waypiedock-master.zip`

### Configuration
Edit `~/.config/waypiedock/waypiedock.ini` based on [this example file](waypiedock.ini)  
*if you were running one of the firsts waypiedock versions and want to copy  
inlined configuration to this ini file, mind the '`s`' in excluded_apps_id.*

### On compositor side
launch waypiedock, then bind those commands to your liking:
 * `printf DOWN > /tmp/.waypiedock.fifo`
 * `printf UP > /tmp/.waypiedock.fifo`

