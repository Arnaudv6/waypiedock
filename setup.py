#!/usr/bin/env python

import setuptools
import codecs
import os.path


def get_version():
    here = os.path.abspath(os.path.dirname(__file__))
    with codecs.open(os.path.join(here, 'waypiedock'), 'r') as fp:
        for line in fp.readlines():
            if line.startswith('SCRIPT_REVISION'):
                delim = '"' if '"' in line else "'"
                return line.split(delim)[1]
        raise RuntimeError("Unable to find version string.")


setuptools.setup(
    name="waypiedock",
    version=get_version(),
    author="Arnaudv6",
    author_email="arnaudv6@free.fr",
    description="A wayland pie-dock.",
    long_description='''A launcher dock, in the shape of a pie, under your mouse. i.e. a wayland piedock.''',
    url="https://gitlab.com/Arnaudv6/waypiedock",
    packages=setuptools.find_packages(),
    license='GNU General Public License v3 (GPLv3)',
    platforms=['posix',],
    scripts=['waypiedock',],
    py_modules=[],
    ext_modules=[],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Operating System :: POSIX",
    ],
    python_requires='>=3.6',
)

